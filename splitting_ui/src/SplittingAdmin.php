<?php

namespace Drupal\splitting_ui;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\splitting_ui\Form\SplittingFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays splitting CSS selector target.
 *
 * @internal
 */
class SplittingAdmin extends FormBase {

  /**
   * Splitting manager.
   *
   * @var \Drupal\splitting_ui\SplittingManagerInterface
   */
  protected $targetManager;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * Constructs a new Animate object.
   *
   * @param \Drupal\splitting_ui\SplittingManagerInterface $target_manager
   *   The Splitting target manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param \Symfony\Component\HttpFoundation\Request $current_request
   *   The current request.
   */
  public function __construct(SplittingManagerInterface $target_manager, DateFormatterInterface $date_formatter, FormBuilderInterface $form_builder, Request $current_request) {
    $this->targetManager  = $target_manager;
    $this->dateFormatter  = $date_formatter;
    $this->formBuilder    = $form_builder;
    $this->currentRequest = $current_request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('splitting.target_manager'),
      $container->get('date.formatter'),
      $container->get('form_builder'),
      $container->get('request_stack')->getCurrentRequest(),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'splitting_admin_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $splitting
   *   (optional) CSS Selector to be added to Splitting.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $splitting = '') {
    $form['#attached']['library'][] = 'splitting_ui/splitting-list';

    $search = $this->currentRequest->query->get('search');
    $status = $this->currentRequest->query->get('status') ?? NULL;

    /** @var \Drupal\splitting_ui\Form\SplittingFilter $form */
    $form['splitting_admin_filter_form'] = $this->formBuilder->getForm(SplittingFilter::class, $search, $status);
    $form['#attributes']['class'][] = 'splitting-filter';
    $form['#attributes']['class'][] = 'views-exposed-form';

    $header = [
      [
        'data'  => $this->t('Target'),
        'field' => 's.sid',
      ],
      [
        'data'  => $this->t('Label'),
        'field' => 's.label',
      ],
      [
        'data'  => $this->t('Status'),
        'field' => 's.status',
      ],
      [
        'data'  => $this->t('Updated'),
        'field' => 's.changed',
        'sort'  => 'desc',
      ],
      $this->t('Operations'),
    ];

    $rows = [];
    $result = $this->targetManager->findAll($header, $search, $status);
    foreach ($result as $target) {
      $row = [];
      $row['target'] = $target->target;
      $row['label'] = $target->label;
      $status_class = $target->status ? 'marker marker--enabled' : 'marker';
      $row['status'] = [
        'data' => [
          '#type' => 'markup',
          '#prefix' => '<span class="' . $status_class . '">',
          '#suffix' => '</span>',
          '#markup' => $target->status ? $this->t('Enabled') : $this->t('Disabled'),
        ],
      ];
      $row['changed'] = $this->dateFormatter->format($target->changed, 'short');
      $links = [];
      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url'   => Url::fromRoute('splitting.edit', ['splitting' => $target->sid]),
      ];
      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url'   => Url::fromRoute('splitting.delete', ['splitting' => $target->sid]),
      ];
      $links['duplicate'] = [
        'title' => $this->t('Duplicate'),
        'url'   => Url::fromRoute('splitting.duplicate', ['splitting' => $target->sid]),
      ];
      $row[] = [
        'data' => [
          '#type'  => 'operations',
          '#links' => $links,
        ],
      ];
      $rows[] = $row;
    }

    $form['splitting_admin_table'] = [
      '#type'   => 'table',
      '#header' => $header,
      '#rows'   => $rows,
      '#empty'  => $this->t('No splitting CSS selector available. <a href=":link">Add target</a> .', [
        ':link' => Url::fromRoute('splitting.add')
          ->toString(),
      ]),
      '#attributes' => ['class' => ['splitting-list']],
    ];

    $form['pager'] = ['#type' => 'pager'];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Add operations to splitting list.
  }

}
