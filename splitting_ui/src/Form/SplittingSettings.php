<?php

namespace Drupal\splitting_ui\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures splitting settings.
 */
class SplittingSettings extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new Splitting object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'splitting_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get current settings.
    $config = $this->config('splitting.settings');

    $form['settings'] = [
      '#type'  => 'details',
      '#title' => $this->t('Splitting settings'),
      '#open'  => TRUE,
    ];

    // Let module handle load Splitting library.
    $form['settings']['load'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Load splitting library'),
      '#default_value' => $config->get('load'),
      '#description'   => $this->t("If enabled, this module will attempt to load the Splitting library for your site. To prevent loading twice, leave this option disabled if you're including the assets manually or through another module or theme."),
    ];

    // Let module handle load Splitting library.
    $form['settings']['rtl'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('RTL language support'),
      '#default_value' => $config->get('rtl'),
      '#description'   => $this->t("Support for connected Persian and Arabic letters."),
    ];

    // Show warning missing library and lock on cdn method.
    $method = $config->get('method');
    $method_lock_change = FALSE;
    if (!splitting_check_installed()) {
      $method = 'cdn';
      $method_lock_change = TRUE;
      $method_warning = $this->t('You cannot set local due to the Splitting.js library is missing. Please <a href=":downloadUrl" rel="external" target="_blank">Download the library</a> and and extract to "/libraries/splitting" directory.', [
        ':downloadUrl' => 'https://github.com/shshaw/Splitting/archive/master.zip',
      ]);

      // Display warning message off.
      $form['settings']['silent'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Turn off warning'),
        '#default_value' => $config->get('silent') ?? FALSE,
        '#description'   => $this->t("If you want to use the CDN without installing the local library, you can turn off the warning."),
      ];

      $form['settings']['method_warning'] = [
        '#type'   => 'item',
        '#markup' => '<div class="library-status-report">' . $method_warning . '</div>',
        '#states' => [
          'visible' => [
            ':input[name="load"]' => ['checked' => TRUE],
          ],
          'invisible' => [
            ':input[name="silent"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    // Load method library from CDN or Locally.
    $form['settings']['method'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Splitting Method'),
      '#options'       => [
        'local' => $this->t('Local'),
        'cdn'   => $this->t('CDN'),
      ],
      '#default_value' => $method,
      '#description'   => $this->t('These settings control how the Splitting library is loaded. You can choose to load from the CDN (External source) or from the local (Internal library).'),
      '#disabled'      => $method_lock_change,
    ];

    // Production or minimized version.
    $form['settings']['minimized'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Development or Production version'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
    ];
    $form['settings']['minimized']['minimized_options'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Choose minimized or non-minimized library.'),
      '#options'       => [
        0 => $this->t('Use non-minimized library (Development)'),
        1 => $this->t('Use minimized library (Production)'),
      ],
      '#default_value' => $config->get('minimized.options'),
      '#description'   => $this->t('These settings work with both local library and CDN methods.'),
    ];

    // Load Splitting library Per-path.
    $form['settings']['url'] = [
      '#type'        => 'fieldset',
      '#title'       => $this->t('Load on specific URLs'),
      '#collapsible' => TRUE,
      '#collapsed'   => TRUE,
    ];
    $form['settings']['url']['url_visibility'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Load splitting on specific pages'),
      '#options'       => [
        0 => $this->t('All pages except those listed'),
        1 => $this->t('Only the listed pages'),
      ],
      '#default_value' => $config->get('url.visibility'),
    ];
    $form['settings']['url']['url_pages'] = [
      '#type'          => 'textarea',
      '#title'         => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
      '#default_value' => _splitting_ui_array_to_string($config->get('url.pages')),
      '#description'   => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %admin-wildcard for every user page. %front is the front page.", [
        '%admin-wildcard' => '/admin/*',
        '%front'          => '<front>',
      ]),
    ];

    // Load Splitting library Per-path.
    $form['options'] = [
      '#type'  => 'details',
      '#title' => $this->t('Splitting default options'),
      '#open'  => TRUE,
    ];

    // List of selectors to individual Splitting Control with Splitting.js.
    $form['options']['targets'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Global targets'),
      '#default_value' => _splitting_ui_array_to_string($config->get('options.target')),
      '#description'   => $this->t('Enter CSS selector (id/class) of your target elements e.g., "#id" or ".classname". Use a new line for each target.'),
      '#rows'          => 3,
    ];

    // The splitting plugin to use.
    $form['options']['by'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Splitting by'),
      '#options'       => _splitting_ui_plugin_options(),
      '#default_value' => $config->get('options.by'),
      '#description'   => $this->t('The splitting plugin to use. If not specified, the value of the data-splitting attribute will be use. If that is not present, the chars plugin will be used.'),
    ];

    // An optional key used as a prefix on CSS Variables.
    $form['options']['key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Splitting key'),
      '#size'          => 15,
      '#maxlength'     => 15,
      '#default_value' => $config->get('options.key'),
      '#description'   => $this->t('An optional key used as a prefix on CSS Variables. For instance when a key of "hero" is used with the chars plugin, it will change the CSS variable "--char-index" to "--hero-char-index". This should be used if multiple splits have been performed on the same element, or to resolve conflicts with other libraries.'),
    ];

    // Load Splitting library Per-path.
    $form['plugin-specific'] = [
      '#type'  => 'details',
      '#title' => $this->t('Plugin-specific options'),
      '#open'  => FALSE,
    ];

    // Used by the following plugins to select children to index.
    $form['plugin-specific']['matching'] = [
      '#title'         => $this->t('Matching'),
      '#type'          => 'textfield',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $config->get('options.matching'),
      '#description'   => $this->t('Used by the following plugins to select children to index: items, grid, cols, rows, cellRows, cellCols, and cells. If not specified, the immediate child elements will be selected.'),
    ];

    // The number of columns to create or detect.
    // This is used by the following plugins: cols, cellCols, grid, and cells.
    $form['plugin-specific']['cols'] = [
      '#title'         => $this->t('Columns'),
      '#type'          => 'textfield',
      '#size'          => 3,
      '#maxlength'     => 3,
      '#default_value' => $config->get('options.cols'),
      '#description'   => $this->t('The number of columns to create or detect. This is used by the following plugins: cols, cellCols, grid, and cells.'),
    ];

    // The number of rows to create or detect.
    // This is used by the following plugins: rows, cellRows, grid, and cells.
    $form['plugin-specific']['rows'] = [
      '#title'         => $this->t('Rows'),
      '#type'          => 'textfield',
      '#size'          => 3,
      '#maxlength'     => 3,
      '#default_value' => $config->get('options.rows'),
      '#description'   => $this->t('The number of rows to create or detect. This is used by the following plugins: rows, cellRows, grid, and cells.'),
    ];

    // If true, the chars plugin will count whitespace while indexing chars.
    $form['plugin-specific']['whitespace'] = [
      '#title'         => $this->t('Whitespace'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('options.whitespace'),
      '#description'   => $this->t('If true, the chars plugin will count whitespace while indexing characters.'),
    ];

    // Cells plugin is perfect for splitting a picture with the image option,
    // which applies an image as a background-image to the target.
    $form['plugin-specific']['image'] = [
      '#title'         => $this->t('image'),
      '#type'          => 'checkbox',
      '#default_value' => $config->get('options.image'),
      '#description'   => $this->t('Cells plugin is perfect for splitting a picture with the image option, which applies an image as a background-image to the target. Set a specific image with image: "http://url-to-image" or use image: true detect the first <img /> in the container for use.'),
    ];

    // Splitting css examples.
    $form['examples'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Style examples'),
      '#open'   => TRUE,
      '#states' => [
        'visible' => [
          'select[name="by"]' => ['value' => 'chars'],
        ],
      ],
    ];

    // Splitting style options.
    $form['examples']['style'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Style'),
      '#options'       => _splitting_ui_style_examples(),
      '#default_value' => $config->get('style'),
      '#description'   => $this->t('These are just a few css style examples to show how to use the splitting plugin.<br><b>Note:</b> Choose "No style" to use own style on your theme css file.'),
    ];

    // Splitting preview markup.
    $markup = '<div class="splitting-preview"><p class="splitting-sample">Everything <b>is</b> contextual!</p></div>';
    $language_direction = $this->languageManager->getCurrentLanguage()->getDirection();
    if ($language_direction == 'rtl') {
      $markup = '<div class="splitting-preview"><p class="splitting-sample">همه‌چیز <b>یک</b> متن میباشد!</p></div>';
    }

    // Splitting style Preview.
    $form['examples']['sample'] = [
      '#type'   => 'markup',
      '#markup' => $markup,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Save the updated Splitting settings.
    $this->config('splitting.settings')
      ->set('load', $values['load'])
      ->set('rtl', $values['rtl'])
      ->set('silent', isset($values['silent']) && $values['silent'] !== 0 ?? FALSE)
      ->set('method', $values['method'])
      ->set('minimized.options', $values['minimized_options'])
      ->set('url.visibility', $values['url_visibility'])
      ->set('url.pages', _splitting_ui_string_to_array($values['url_pages']))
      ->set('options.target', _splitting_ui_string_to_array($values['targets']))
      ->set('options.by', $values['by'])
      ->set('options.key', $values['key'])
      ->set('options.matching', $values['matching'])
      ->set('options.cols', $values['cols'])
      ->set('options.rows', $values['rows'])
      ->set('options.whitespace', $values['whitespace'])
      ->set('options.image', $values['image'])
      ->set('style', $values['style'])
      ->save();

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'splitting.settings',
    ];
  }

}
