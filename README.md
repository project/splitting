INTRODUCTION
------------

This module integrates the 'Splitting' plugin - https://splitting.js.org/,
splitting.js, creates elements and adds CSS variables for split words & chars!.


FEATURES
--------

'Splitting' plugin is:

  - Lightweight

  - Easy to use

  - Responsive

  - Customizable


REQUIREMENTS
------------

'Splitting' plugin - https://github.com/shshaw/Splitting/archive/master.zip


INSTALLATION
------------

1. Download 'Splitting' module - https://www.drupal.org/project/splitting

2. Extract and place it in the root modules directory i.e.
   /modules/contrib/splitting

3. Create a libraries directory in the root, if not already there i.e.
   /libraries

4. Create a splitting directory inside it i.e.
   /libraries/splitting

5. Download 'Splitting.js' plugin
   https://github.com/shshaw/Splitting/archive/master.zip

6. Place it in the /libraries/splitting directory i.e. Required files:

  - /libraries/splitting/dist/splitting.min.js
  - /libraries/splitting/dist/splitting.css
  - /libraries/splitting/dist/splitting-cells.css

7. Now, enable 'splitting' module


USAGE
-----

Splitting can be called without parameters, automatically splitting all
elements with data-splitting attributes by the default of chars which wraps
the element's text in <span>s with relevant CSS vars.

The splitting chars plugin splits an element's text into separate characters.
Before it can run, it splits by words to prevent issues with text wrapping.
chars is the default plugin if no other plugin is specified.


Example Code
============

### HTML:

<div id="target">Splitting Text</div>

### JAVASCRIPT:

Then a script block with the magical Splitting() method:

<script>
const target = document.querySelector('#target');
const results = Splitting({
  target: target,
  by: 'chars'
});
</script>

### RESULT:

The resulting code will churn your #target and output the following:

<div id="target" data-once="splitting" class="splitting words chars"
     style="--word-total: 2; --char-total: 13;">
    <span class="word" data-word="Splitting" style="--word-index: 0;">
        <span class="char" data-char="S" style="--char-index: 0;">S</span>
        <span class="char" data-char="p" style="--char-index: 1;">p</span>
        <span class="char" data-char="l" style="--char-index: 2;">l</span>
        <span class="char" data-char="i" style="--char-index: 3;">i</span>
        <span class="char" data-char="t" style="--char-index: 4;">t</span>
        <span class="char" data-char="t" style="--char-index: 5;">t</span>
        <span class="char" data-char="i" style="--char-index: 6;">i</span>
        <span class="char" data-char="n" style="--char-index: 7;">n</span>
        <span class="char" data-char="g" style="--char-index: 8;">g</span>
    </span>
    <span class="whitespace"> </span>
    <span class="word" data-word="Text" style="--word-index: 1;">
        <span class="char" data-char="T" style="--char-index: 9;">T</span>
        <span class="char" data-char="e" style="--char-index: 10;">e</span>
        <span class="char" data-char="x" style="--char-index: 11;">x</span>
        <span class="char" data-char="t" style="--char-index: 12;">t</span>
    </span>
</div>

Also, you can use other Splitting plugins such as word, lines etc.
For learn more usage check document on "https://splitting.js.org/guide.html"
official Splitting.js website.


How does it Work?
=================

1. Enable splitting module, Follow INSTALLATION in above.

2. Pick a css selector as a Splitting target like:

.page-title
.block-title
.node__title
e.g.,

3. Call splitting in your theme Script file, Follow USAGE in above.

4. Add your style in your theme CSS file with some selector:

5. Enjoy that.

'Splitting' module will split every word and char to a span for
radical Web Typography.


MAINTAINERS
-----------

Current module maintainer:

 * Mahyar Sabeti - https://www.drupal.org/u/mahyarsbt


DEMO
====
https://splitting.js.org/
