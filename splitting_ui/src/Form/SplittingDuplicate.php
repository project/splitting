<?php

namespace Drupal\splitting_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\splitting_ui\SplittingManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to duplicate splitting.
 *
 * @internal
 */
class SplittingDuplicate extends FormBase {

  /**
   * The splitting id.
   *
   * @var int
   */
  protected $splitting;

  /**
   * The Splitting target manager.
   *
   * @var \Drupal\splitting_ui\SplittingManagerInterface
   */
  protected $splittingManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new SplittingDuplicate object.
   *
   * @param \Drupal\splitting_ui\SplittingManagerInterface $splitting_manager
   *   The splitting target manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(SplittingManagerInterface $splitting_manager, TimeInterface $time) {
    $this->splittingManager = $splitting_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('splitting.target_manager'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'splitting_duplicate_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $splitting
   *   The splitting record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $splitting = 0) {
    $form['splitting_id'] = [
      '#type'  => 'value',
      '#value' => $splitting,
    ];

    // New target to duplicate splitting.
    $form['target'] = [
      '#title'         => $this->t('Target'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 255,
      '#default_value' => '',
      '#description'   => $this->t('Here, you can use HTML tag, class with dot(.) and ID with hash(#) prefix. Be sure your selector has plain text content. e.g. ".page-title" or ".block-title".'),
      '#placeholder'   => $this->t('Enter valid selector'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#button_type' => 'primary',
      '#value'       => $this->t('Duplicate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $sid    = $form_state->getValue('splitting_id');
    $is_new = $sid == 0;
    $target = trim($form_state->getValue('target'));

    if ($is_new) {
      if ($this->splittingManager->isSplitting($target)) {
        $form_state->setErrorByName('target', $this->t('This target is already exists.'));
      }
    }
    else {
      if ($this->splittingManager->findById($sid)) {
        $splitting = $this->splittingManager->findById($sid);

        if ($target != $splitting['target'] && $this->splittingManager->isSplitting($target)) {
          $form_state->setErrorByName('target', $this->t('This target is already added.'));
        }
      }
    }
  }

  /**
   * Form submission handler for the 'duplicate' action.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   A reference to a keyed array containing the current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $sid    = $values['splitting_id'];
    $target = trim($values['target']);
    $label  = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $target)));
    $status = 1;

    $splitting = $this->splittingManager->findById($sid);
    $comment = $splitting['comment'];
    $options = $splitting['options'];

    // The Unix timestamp when the splitting was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Save splitting.
    $new_sid = $this->splittingManager->addSplitting(0, $target, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The target %target has been duplicated.', ['%target' => $target]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();

    // Redirect to duplicated splitting edit form.
    $form_state->setRedirect('splitting.edit', ['splitting' => $new_sid]);
  }

}
