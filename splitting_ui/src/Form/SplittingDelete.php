<?php

namespace Drupal\splitting_ui\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\splitting_ui\SplittingManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a form to remove CSS selector.
 *
 * @internal
 */
class SplittingDelete extends ConfirmFormBase {

  /**
   * The Splitting target.
   *
   * @var int
   */
  protected $splitting;

  /**
   * The Splitting target manager.
   *
   * @var \Drupal\splitting_ui\SplittingManagerInterface
   */
  protected $targetManager;

  /**
   * Constructs a new SplittingDelete object.
   *
   * @param \Drupal\splitting_ui\SplittingManagerInterface $target_manager
   *   The Splitting target manager.
   */
  public function __construct(SplittingManagerInterface $target_manager) {
    $this->targetManager = $target_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('splitting.target_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'splitting_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to remove %target from splitting targets?', ['%target' => $this->splitting['target']]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param string $splitting
   *   The Splitting record ID to remove.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $splitting = '') {
    if (!$this->splitting = $this->targetManager->findById($splitting)) {
      throw new NotFoundHttpException();
    }
    $form['splitting_id'] = [
      '#type'  => 'value',
      '#value' => $splitting,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $splitting_id = $form_state->getValue('splitting_id');
    $this->targetManager->removeSplitting($splitting_id);
    $this->logger('user')
      ->notice('Deleted %target', ['%target' => $this->splitting['target']]);
    $this->messenger()
      ->addStatus($this->t('The Splitting target %target was deleted.', ['%target' => $this->splitting['target']]));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('splitting.admin');
  }

}
