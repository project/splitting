/**
 * @file
 * Contains definition of the behaviour SplittingJS.
 */

(function ($, Drupal, drupalSettings, once, Splitting) {
  "use strict";

  Drupal.behaviors.splittingJS = {
    attach: function (context, settings) {

      const style = drupalSettings.splitting.style;
      const example = drupalSettings.splitting.sample;

      if (once('splitting-sample', example.target).length) {
        let spltTarget = example.target;
        let spltBy = example.by;
        let spltKey = example.key;
        let spltMatch = example.matching;
        let spltCols = example.cols;
        let spltRows = example.rows;
        let spltWSpace = example.whitespace;
        let spltImage = example.image;

        Splitting({
          target: spltTarget,
          by: spltBy,
          key: spltKey,
        });

        $(spltTarget).addClass(style);

        $('select#edit-style').on(
          'change',
          function (event) {
            let lastClass = $(spltTarget).attr('class').split(' ').pop();
            if (lastClass !== 'splitting') {
              $(spltTarget).removeClass(lastClass);
            }
            $(spltTarget).addClass(this.value);
          }).trigger('change');
      }

      if (once('splitting__about', '.splitting__text').length) {
        new Drupal.splittingHelp();
      }

    }
  };

  Drupal.splittingHelp = function () {
    Splitting({
      target: '.page-title',
      by: 'chars'
    });
    $('.page-title').addClass('color');

    Splitting({
      target: '.splitting__text',
      by: 'chars'
    });

    Splitting({
      target: '.block h3:last-of-type',
      by: 'chars'
    });
    $(".block h3:last-of-type").addClass('paper');

  }

})(jQuery, Drupal, drupalSettings, once, Splitting);
