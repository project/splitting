<?php

namespace Drupal\splitting_ui;

/**
 * Provides an interface defining a Splitting manager.
 */
interface SplittingManagerInterface {

  /**
   * Returns if this Splitting target is added.
   *
   * @param string $target
   *   The Splitting target to check.
   *
   * @return bool
   *   TRUE if the Splitting target is added, FALSE otherwise.
   */
  public function isSplitting($target);

  /**
   * Finds all enabled splitting.
   *
   * @return string|false
   *   Either the enabled splitting or FALSE if none exist.
   */
  public function loadSplitting();

  /**
   * Add a Splitting target.
   *
   * @param int $splitting_id
   *   The Splitting id for edit.
   * @param string $target
   *   The Splitting target to add.
   * @param string $label
   *   The label of splitting target.
   * @param string $comment
   *   The comment for splitting options.
   * @param int $changed
   *   The expected modification time.
   * @param int $status
   *   The status for splitting.
   * @param string $options
   *   The Splitting target options.
   *
   * @return int|null|string
   *   The last insert ID of the query, if one exists.
   */
  public function addSplitting($splitting_id, $target, $label, $comment, $changed, $status, $options);

  /**
   * Remove a Splitting target.
   *
   * @param int $splitting_id
   *   The Splitting id to remove.
   */
  public function removeSplitting($splitting_id);

  /**
   * Finds all added Splitting target.
   *
   * @param array $header
   *   The splitting header to sort target and label.
   * @param string $search
   *   The splitting search key to filter target.
   * @param int|null $status
   *   The splitting status to filter target.
   *
   * @return \Drupal\Core\Database\StatementInterface
   *   The result of the database query.
   */
  public function findAll(array $header, $search, $status);

  /**
   * Finds an added Splitting target by its ID.
   *
   * @param int $splitting_id
   *   The ID for an added Splitting target.
   *
   * @return string|false
   *   Either the added Splitting target or FALSE if none exist with that ID.
   */
  public function findById($splitting_id);

}
