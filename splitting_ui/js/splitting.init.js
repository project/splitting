/**
 * @file
 * Contains definition of the behaviour SplittingJS.
 */

(function ($, Drupal, drupalSettings, once, Splitting) {
  "use strict";

  Drupal.behaviors.splittingJS = {
    attach: function (context, settings) {

      const elements = drupalSettings.splitting.elements;
      const style = drupalSettings.splitting.style;

      $.each(elements, function (index, option) {
        let spltTarget = option.target;
        let spltBy = option.by;
        let spltKey = option.key;
        let spltMatch = option.matching;
        let spltCols = option.cols;
        let spltRows = option.rows;
        let spltWSpace = option.whitespace;
        let spltImage = option.image;
        let spltStyle = option.style;

        if (Array.isArray(spltTarget)) {
          $.each(spltTarget, function (index, target) {
            if (once('splitting', target).length) {
              let results = Splitting({
                target: target,
                by: spltBy,
                key: spltKey,
                matching: spltMatch,
                cols: spltCols,
                rows: spltRows,
                whitespace: spltWSpace,
                image: spltImage,
              });
              if (style) {
                $(target).addClass(style);
              }
            }
          });
        }
        else {
          if (once('splitting', spltTarget).length) {
            let results = Splitting({
              target: spltTarget,
              by: spltBy,
              key: spltKey,
              matching: spltMatch,
              cols: spltCols,
              rows: spltRows,
              whitespace: spltWSpace,
              image: spltImage,
            });
            if (spltStyle) {
              $(spltTarget).addClass(spltStyle);
            }
          }
        }
      });

    }
  };

})(jQuery, Drupal, drupalSettings, once, Splitting);
