<?php

namespace Drupal\splitting_ui\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\splitting_ui\SplittingManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Splitting add and edit targets form.
 *
 * @internal
 */
class SplittingForm extends FormBase {

  /**
   * Splitting manager.
   *
   * @var \Drupal\splitting_ui\SplittingManagerInterface
   */
  protected $splittingManager;

  /**
   * A config object for the system performance configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new Splitting object.
   *
   * @param \Drupal\splitting_ui\SplittingManagerInterface $splitting_manager
   *   The add Splitting manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(SplittingManagerInterface $splitting_manager, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager, TimeInterface $time) {
    $this->splittingManager = $splitting_manager;
    $this->config = $config_factory->get('splitting.settings');
    $this->languageManager = $language_manager;
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('splitting.target_manager'),
      $container->get('config.factory'),
      $container->get('language_manager'),
      $container->get('datetime.time'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'splitting_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param int $splitting
   *   (optional) Splitting id to be passed on to
   *   \Drupal::formBuilder()->getForm() for use as the default value of the
   *   Splitting ID form data.
   */
  public function buildForm(array $form, FormStateInterface $form_state, int $splitting = 0) {

    $sid       = $splitting;
    $splitting = $this->splittingManager->findById($splitting) ?? [];
    $target    = $splitting['target'] ?? '';
    $label     = $splitting['label'] ?? '';
    $comment   = $splitting['comment'] ?? '';
    $status    = $splitting['status'] ?? TRUE;
    $options   = [];

    // Handle the case when $splitting is not an array or option is not set.
    if (is_array($splitting) && isset($splitting['options'])) {
      $options = unserialize($splitting['options'], ['allowed_classes' => FALSE]) ?? '';
    }

    // Store splitting id.
    $form['splitting_id'] = [
      '#type'  => 'value',
      '#value' => $sid,
    ];

    // Load the Splitting configuration settings.
    $config = $this->config;

    // The default target to use when detecting multiple splitting.
    $form['target'] = [
      '#title'         => $this->t('Target'),
      '#type'          => 'textfield',
      '#required'      => TRUE,
      '#size'          => 64,
      '#maxlength'     => 256,
      '#default_value' => $target,
      '#description'   => $this->t('Enter a valid element or a css selector.'),
    ];

    // The label of this selector.
    $form['label'] = [
      '#title'         => $this->t('Label'),
      '#type'          => 'textfield',
      '#required'      => FALSE,
      '#size'          => 64,
      '#maxlength'     => 64,
      '#default_value' => $label ?? '',
      '#description'   => $this->t('The label for this splitting target like <em>Frontpage title</em>.'),
    ];

    // Splitting settings.
    $form['options'] = [
      '#title' => $this->t('Splitting options'),
      '#type'  => 'details',
      '#open'  => TRUE,
    ];

    // The splitting plugin to use.
    $form['options']['by'] = [
      '#title'         => $this->t('Splitting by'),
      '#type'          => 'select',
      '#options'       => _splitting_ui_plugin_options(),
      '#default_value' => $options['by'] ?? $config->get('options.by'),
      '#description'   => $this->t('The splitting plugin to use. If not specified, the value of the data-splitting attribute will be use. If that is not present, the chars plugin will be used.'),
    ];

    // An optional key used as a prefix on CSS Variables.
    $form['options']['key'] = [
      '#title'         => $this->t('Splitting key'),
      '#type'          => 'textfield',
      '#size'          => 15,
      '#maxlength'     => 15,
      '#default_value' => $options['key'] ?? $config->get('options.key'),
      '#description'   => $this->t('An optional key used as a prefix on CSS Variables. For instance when a key of "hero" is used with the chars plugin, it will change the CSS variable "--char-index" to "--hero-char-index". This should be used if multiple splits have been performed on the same element, or to resolve conflicts with other libraries.'),
    ];

    // Load Splitting library Per-path.
    $form['plugin-specific'] = [
      '#title' => $this->t('Plugin-specific options'),
      '#type'  => 'details',
      '#open'  => FALSE,
    ];

    // Used by the following plugins to select children to index.
    $form['plugin-specific']['matching'] = [
      '#title'         => $this->t('Matching'),
      '#type'          => 'textfield',
      '#size'          => 9,
      '#maxlength'     => 9,
      '#default_value' => $options['matching'] ?? $config->get('options.matching'),
      '#description'   => $this->t('Used by the following plugins to select children to index: items, grid, cols, rows, cellRows, cellCols, and cells. If not specified, the immediate child elements will be selected.'),
    ];

    // The number of columns to create or detect.
    // This is used by the following plugins: cols, cellCols, grid, and cells.
    $form['plugin-specific']['cols'] = [
      '#title'         => $this->t('Columns'),
      '#type'          => 'number',
      '#size'          => 3,
      '#maxlength'     => 3,
      '#default_value' => $options['cols'] ?? $config->get('options.cols'),
      '#description'   => $this->t('The number of columns to create or detect. This is used by the following plugins: cols, cellCols, grid, and cells.'),
    ];

    // The number of rows to create or detect.
    // This is used by the following plugins: rows, cellRows, grid, and cells.
    $form['plugin-specific']['rows'] = [
      '#title'         => $this->t('Rows'),
      '#type'          => 'number',
      '#size'          => 3,
      '#maxlength'     => 3,
      '#default_value' => $options['rows'] ?? $config->get('options.rows'),
      '#description'   => $this->t('The number of rows to create or detect. This is used by the following plugins: rows, cellRows, grid, and cells.'),
    ];

    // If true, the chars plugin will count whitespace while indexing chars.
    $form['plugin-specific']['whitespace'] = [
      '#title'         => $this->t('Whitespace'),
      '#type'          => 'checkbox',
      '#default_value' => $options['whitespace'] ?? $config->get('options.whitespace'),
      '#description'   => $this->t('If true, the chars plugin will count whitespace while indexing characters.'),
    ];

    // Cells plugin is perfect for splitting a picture with the image option,
    // which applies an image as a background-image to the target.
    $form['plugin-specific']['image'] = [
      '#title'         => $this->t('Image'),
      '#type'          => 'checkbox',
      '#default_value' => $options['image'] ?? $config->get('options.image'),
      '#description'   => $this->t('Cells plugin is perfect for splitting a picture with the image option, which applies an image as a background-image to the target. Set a specific image with image: "http://url-to-image" or use image: true detect the first <img /> in the container for use.'),
    ];

    // Splitting css examples.
    $form['examples'] = [
      '#type'   => 'details',
      '#title'  => $this->t('Style examples'),
      '#open'   => TRUE,
      '#states' => [
        'visible' => [
          'select[name="by"]' => ['value' => 'chars'],
        ],
      ],
    ];

    // Splitting style options.
    $form['examples']['style'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Style'),
      '#options'       => _splitting_ui_style_examples(),
      '#default_value' => $options['style'] ?? $config->get('style'),
      '#description'   => $this->t('These are just a few css style examples to show how to use the splitting plugin.<br><b>Note:</b> Choose "No style" to use own style on your theme css file.'),
    ];

    // Splitting preview markup.
    $markup = '<div class="splitting-preview"><p class="splitting-sample">Everything <b>is</b> contextual!</p></div>';
    $language_direction = $this->languageManager->getCurrentLanguage()->getDirection();
    if ($language_direction == 'rtl') {
      $markup = '<div class="splitting-preview"><p class="splitting-sample">همه‌چیز <b>یک</b> متن میباشد!</p></div>';
    }

    // Splitting style Preview.
    $form['examples']['sample'] = [
      '#type'   => 'markup',
      '#markup' => $markup,
    ];

    // The comment for describe splitting settings and usage in website.
    $form['comment'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Comment'),
      '#default_value' => $comment ?? '',
      '#description'   => $this->t('Describe this splitting settings and usage in your website.'),
      '#rows'          => 2,
      '#weight'        => 96,
    ];

    // Enabled status for this splitting.
    $form['status'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#description'   => $this->t('If enabled, Splitting will work on pages that have this target.'),
      '#default_value' => $status ?? FALSE,
      '#weight'        => 99,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
      '#submit'      => [[$this, 'submitForm']],
    ];

    if ($sid != 0) {
      // Add a 'Remove' button for splitting form.
      $form['actions']['delete'] = [
        '#type'       => 'link',
        '#title'      => $this->t('Delete'),
        '#url'        => Url::fromRoute('splitting.delete', ['splitting' => $sid]),
        '#attributes' => [
          'class' => [
            'action-link',
            'action-link--danger',
            'action-link--icon-trash',
          ],
        ],
      ];

      // Redirect to list for submit handler on edit form.
      $form['actions']['submit']['#submit'] = ['::submitForm', '::overview'];
    }
    else {
      // Add a 'Save and go to list' button for add form.
      $form['actions']['overview'] = [
        '#type'   => 'submit',
        '#value'  => $this->t('Save and go to list'),
        '#submit' => array_merge($form['actions']['submit']['#submit'], ['::overview']),
        '#weight' => 20,
      ];
    }

    return $form;
  }

  /**
   * Submit handler for removing target.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function remove(array &$form, FormStateInterface $form_state) {
    $splitting_id = $form_state->getValue('splitting_id');
    $form_state->setRedirect('splitting.delete', ['splitting' => $splitting_id]);
  }

  /**
   * Form submission handler for the 'overview' action.
   *
   * @param array[] $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function overview(array $form, FormStateInterface $form_state): void {
    $form_state->setRedirect('splitting.admin');
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $sid    = $form_state->getValue('splitting_id');
    $is_new = $sid == 0;
    $target = trim($form_state->getValue('target'));

    if ($is_new) {
      if ($this->splittingManager->isSplitting($target)) {
        $form_state->setErrorByName('target', $this->t('This target is already added.'));
      }
    }
    else {
      if ($this->splittingManager->findById($sid)) {
        $splitting = $this->splittingManager->findById($sid);

        if ($target != $splitting['target'] && $this->splittingManager->isSplitting($target)) {
          $form_state->setErrorByName('target', $this->t('This target is already added.'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get all form field values.
    $values = $form_state->getValues();

    // Set splitting form fields data values.
    $sid     = $values['splitting_id'];
    $target  = trim($values['target']);
    $label   = trim($values['label']);
    $comment = trim($values['comment']);
    $status  = $values['status'];

    // Provide a label from selector if was empty.
    if (empty($label)) {
      $label = ucfirst(trim(preg_replace("/[^a-zA-Z0-9]+/", " ", $target)));
    }

    // Set variables from main splitting settings.
    $variables['by']         = $values['by'];
    $variables['key']        = $values['key'];
    $variables['matching']   = $values['matching'];
    $variables['cols']       = $values['cols'];
    $variables['rows']       = $values['rows'];
    $variables['whitespace'] = $values['whitespace'];
    $variables['image']      = $values['image'];
    $variables['style']      = $values['style'];

    // Serialize options variables.
    $options = serialize($variables);

    // The Unix timestamp when the splitting was most recently saved.
    $changed = $this->time->getCurrentTime();

    // Save splitting.
    $this->splittingManager->addSplitting($sid, $target, $label, $comment, $changed, $status, $options);
    $this->messenger()
      ->addStatus($this->t('The target %target has been added.', ['%target' => $target]));

    // Flush caches so the updated config can be checked.
    drupal_flush_all_caches();
  }

}
